package main

import (
	"fmt"
	"net/http"
	"gitlab.com/redmoosetech1/rmt-oss/dominion/pkg/routes"
)


func main() {
	fmt.Println("Starting Web Application")

	r := routes.NewRouter()
	r.ConfigureV1Routes()

	_ = http.ListenAndServe(":8000", r.Mux)
}
