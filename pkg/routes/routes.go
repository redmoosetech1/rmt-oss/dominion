package routes

import (
	"github.com/go-chi/chi/middleware"
	chi "github.com/go-chi/chi/v5"
	apiV1 "gitlab.com/redmoosetech1/rmt-oss/dominion/pkg/api/v1"
)

type Router struct {
	Mux chi.Router
}

func NewRouter() *Router {
	mux := chi.NewRouter()
	return &Router{
		Mux: mux,
	}
}

func (r *Router) ConfigureV1Routes() {
	r.Mux.Use(middleware.Logger)
	r.Mux.Get("/api/v1/home", apiV1.Home)
	r.Mux.Get("/api/v1/contact", apiV1.Contacts)
}
