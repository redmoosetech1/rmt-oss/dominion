package repos

import (
	"gitlab.com/redmoosetech1/rmt-oss/dominion/pkg/config"
)

type Repository struct {
	App *config.AppConfig
}

type Repo interface {
	Create(*interface{})
	Get(*interface{})
	Update(*interface{})
	Delete(*interface{})
}

func NewRepo(a *config.AppConfig) *Repository {
	return &Repository{
		App: a,
	}
}
