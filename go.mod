module gitlab.com/redmoosetech1/rmt-oss/dominion

go 1.20

require (
	github.com/go-chi/chi v1.5.5
	github.com/go-chi/chi/v5 v5.0.10
)
